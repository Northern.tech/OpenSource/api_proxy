# API Proxy POC

Copyright 2019 Northern.tech AS.

## Installation

Install rust tooling:

```
$ curl https://sh.rustup.rs -sSf | sh
```


## Build

```
$ cargo build
```

## Run

```
$ cargo run
```

## UI

http://127.0.0.1:3000
