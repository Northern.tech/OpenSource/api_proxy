extern crate serde_json;
extern crate futures;
extern crate tokio_fs;
extern crate tokio_io;
extern crate hyper;

use std::sync::{Mutex, Arc};

use futures::{future, Future, Stream};
use serde_json::{Value, to_string, from_str};
use tokio_fs::file::File;
use tokio_io::io::read_to_end;
use tokio_io::AsyncWrite;
use hyper::{rt, header, Body, Method, Response, Request, Server, StatusCode, Client};
use hyper::service::service_fn;

type GenericError = Box<dyn std::error::Error + Send + Sync>;
type ResponseFuture = Box<Future<Item=Response<Body>, Error=GenericError> + Send>;

fn read_to_response(file: File) -> ResponseFuture {
    let buf: Vec<u8> = Vec::new();
    Box::new(
        read_to_end(file, buf)
            .and_then(|item| {
                Ok(Response::new(item.1.into()))
            })
            .or_else(|_| {
                let response = Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::empty())?;
                Ok(response)
            }))
}

fn file_response(f: &str) -> ResponseFuture {
    let filename = f.to_string();
    Box::new(File::open(filename)
        .from_err()
        .and_then(|file| {
            read_to_response(file)
        })
        .or_else(|_| {
            let response = Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body("NOT_FOUND".into())?;
            Ok(response)
        }))
}

fn html(_req: Request<Body>) -> ResponseFuture  {
    file_response("web/index.html")
}

fn save_state(data: Value, state: Arc<Mutex<Value>>) -> String {
    let string = to_string(&data).unwrap();
    let string_copy = string.clone();
    let task = File::create("config.json")
        .and_then(move |mut file| file.poll_write(string.as_bytes()))
        .map(|res| {
            println!("Wrote config.json - {:?}", res);
        }).map_err(|err| eprintln!("IO error: {:?}", err));

    rt::spawn(task);

    *state.lock().unwrap() = data;
    return string_copy;
}

fn post_api(req: Request<Body>, state: Arc<Mutex<Value>>) -> ResponseFuture  {
    Box::new(req.into_body()
        .concat2()
        .from_err()
        .and_then(move |chunks| {
            let str = String::from_utf8(chunks.to_vec())?;
            let data : Value = from_str(&str)?;
            let json = save_state(data, state);
            let response = Response::builder()
                .status(StatusCode::OK)
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(json))?;
            Ok(response)
        })
    )
}

fn fetch_state_pretty(state: Arc<Mutex<Value>>) -> String {
    let value : &Value = &*(state.lock().unwrap());
    serde_json::to_string_pretty(&value).unwrap()
}

fn state_responder(state: Arc<Mutex<Value>>) -> ResponseFuture {
    let string = fetch_state_pretty(state);
    let s = String::from(format!("{}", string));
    Box::new(future::ok(Response::builder()
                        .header(header::CONTENT_TYPE, "application/json")
                        .body(Body::from(s))
                        .unwrap()))
}

fn ui_responder(req: Request<Body>, state: Arc<Mutex<Value>>) -> ResponseFuture  {
    return match (req.method(), req.uri().path()) {
        (&Method::POST, "/state") => {
            post_api(req, state)
        }
        (&Method::GET, "/state") => {
            state_responder(state)
        }
        _ => {
            html(req)
        }
    };
}

fn cache_insert(method: &str, path: &str, body: &str, state: Arc<Mutex<Value>>) {
    let mut state = state.lock().unwrap();
    state["cache"][method][path] = Value::String(body.to_string());
}

fn proxy(req: Request<Body>, state: Arc<Mutex<Value>>, save: bool) -> ResponseFuture  {
    let client = Client::new();
    let proxy_target = "127.0.0.1:8000";
    let req_path = req.uri().path_and_query().map(|x| x.as_str()).unwrap_or("/");
    let uri_string = format!("http://{}{}", proxy_target, req_path);
    let uri = uri_string.parse().unwrap();
    let path = req.uri().path().to_string();
    let method = req.method().to_string();
    Box::new(
        client
            .get(uri)
            .and_then(move |response| {
                response.into_body()
                    .concat2()
                    .from_err()
                    .and_then(move |body| {
                        let str = String::from_utf8(body.to_vec()).unwrap();
                        if save {
                            cache_insert(&method, &path, &str, state);
                        }
                        let response = Response::builder()
                            .status(StatusCode::OK)
                            .body(Body::from(str)).unwrap();
                        Ok(response)
                    })
            })
            .or_else(|_| {
                let response = Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(Body::empty())?;
                Ok(response)
            }))
}

fn should_cache(req: &Request<Body>,  state: &Arc<Mutex<Value>>) -> bool {
    let state : &Value = &*(state).lock().unwrap();
    let config = &state["config"];
    let request_path = req.uri().path();
    let request_method = req.method().to_string();
    match *config {
        Value::Array(ref config) => {
            for row in config {
                let method = &row["method"];
                let path = &row["path"];
                match (method, path) {
                    (Value::String(m), Value::String(p)) => {
                        if *m == request_method && p == request_path {
                            return true;
                        }
                    }
                    _ => {}
                };
            }
            false
        }
        _ => { false }
    }
}

fn cache_lookup(req: &Request<Body>, state: &Arc<Mutex<Value>>) -> Option<String> {
    let method = match *req.method() {
        Method::GET => { "GET" }
        Method::POST => { "POST" }
        _ => { return None; }
    };

    let path = req.uri().path();

    let state : &Value = &*(state).lock().unwrap();
    let target = &state["cache"][method][path];
    match *target {
        Value::String(ref s) => {
            return Some(s.to_string());
        }
        _ => { }
    }
    return None;
}

fn cache_reconstruct(body: String) -> ResponseFuture {
    Box::new(future::ok(Response::builder()
                        .body(Body::from(body))
                        .unwrap()))
}

fn proxy_responder(req: Request<Body>, state: Arc<Mutex<Value>>) -> ResponseFuture  {
    if should_cache(&req, &state)
    {
        let data = cache_lookup(&req, &state);
        match data {
            Some(d) => {
                return cache_reconstruct(d);
            }
            None => {
                return proxy(req, state, true);
            }
        }
    }
    proxy(req, state, false)
}

fn read_state_config() -> String {
    match std::fs::read_to_string("config.json") {
        Ok(d) => {d}
        Err(_) => {String::from("{}")}
    }
}

fn main() {
    let state_str = read_state_config();
    let state : Arc<Mutex<Value>> = Arc::new(Mutex::new(from_str(&state_str).unwrap()));
    let state_ui = state.clone();
    let state_proxy = state.clone();

    let ui_service = move || {
        let state = state_ui.clone();
        service_fn(move |req| {
            ui_responder(req, state.clone())
        })
    };
    let proxy_service = move || {
        let state = state_proxy.clone();
        service_fn(move |req| {
            proxy_responder(req, state.clone())
        })
    };

    let ui_addr = ([127, 0, 0, 1], 3000).into();
    let proxy_addr = ([127, 0, 0, 1], 3001).into();

    hyper::rt::run(rt::lazy(move || {
        let ui_server = Server::bind(&ui_addr)
            .serve(ui_service)
            .map_err(|e| eprintln!("UI Server error: {}", e));
        println!("UI Server on http://{} ", ui_addr);

        let proxy_server = Server::bind(&proxy_addr)
            .serve(proxy_service)
            .map_err(|e| eprintln!("Proxy Server error: {}", e));
        println!("Proxy Server on http://{} ", proxy_addr);

        rt::spawn(ui_server);
        rt::spawn(proxy_server);

        return Ok(());
    }));
}
